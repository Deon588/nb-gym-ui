    # What is this repository for?

    * Nightsbridge Gym booking assessment frontend
    * Version: 1.0.0


    ## Tech stack
    
    ### Languages
    * [Typescript](https://www.typescriptlang.org/)
    * [HTML5](https://html.com › html5)
    * [SASS](https://sass-lang.com/)
    ### Framework
    * [Angular 10](https://angular.io/)
    
    
    ### Build tool
    * [Angular CLI](https://cli.angular.io/)
    
    ### Other libraries
    * [Jest](https://github.com/facebook/jest)
    * [Spectator](https://github.com/ngneat/spectator)
    * [Angular Material](https://material.angular.io/)
    * [MomentJS](https://material.angular.io/)
    * [@angular-material-components/datetime-picker](https://www.npmjs.com/package/@angular-material-components/datetime-picker)
    

    ## How do I get set up?

    ### How to build
    * After cloning cd into project directory and run ng build
    ### How to run
    * From project directory run npm start (Don't run ng serve as i'm using a proxy to avoid CORS issues + serving over https)
    ### How to run tests
    * To run tests normally run npm run test.
    * To run test in slower debug mode run npm run test:debug.
    * To generate test coverage report run npm run test:coverage.
    

    ## Assumptions/noteworthy mentions #    
    * This is a very minimalistic UI as I had quite a busy past sprint however I think all requirements are satisfied.    
    * My laptop is completely locked down (on the Vodacom domain) and i'm only able to navigate to unfiltered web URLs and I don't have Visio installed therefore I couldn't draw any design diagrams...
    * I only managed to test the BookingService clasws.

     ### Code smells/bad practices 
    * The way I handle filtering by trainerId is a bit of a hack however I ran out of time so made the best of the time left...    
    * Also the way I handle displaying/hiding the loader component is not optimal, I generally prefer using ngrx for state management however thought it would be overkill for this assessment.
    
    
    