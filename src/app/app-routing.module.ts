import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AvailableGymClassesComponent } from './gym-classes/available-gym-classes/available-gym-classes.component';

const routes: Routes = [
  { path: 'gymclasses/available', component: AvailableGymClassesComponent },  
  { path: '', redirectTo: 'gymclasses/available', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
