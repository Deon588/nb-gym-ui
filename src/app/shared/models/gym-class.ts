export class GymClass {
    id: number;
    name: string;
    description: string;
    durationInHours: number;
    trainer: string;

    
    constructor(id: number, name: string, description: string, durationInHours: number, trainer: string) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.durationInHours = durationInHours;
        this.trainer = trainer;
    }
}
