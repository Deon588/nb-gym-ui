export class Trainer {
    id: number;
    firstName: string;
    lastName: string;
    age: number;
    eamail: string;

    constructor(id: number, firstName: string, lastName: string, age: number, email: string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.eamail = email;
    }

}
