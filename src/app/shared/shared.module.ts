import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';

import { LoaderComponent } from './components/loader/loader.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';

import { BookingService } from './services/booking.service';



@NgModule({  
  declarations: [
    LoaderComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatToolbarModule
  ],
  providers: [
    BookingService
  ],
  exports:[
    LoaderComponent,
    ToolbarComponent
  ]
})
export class SharedModule { }
