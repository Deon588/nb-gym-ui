import { createHttpFactory, HttpMethod, SpectatorHttp } from '@ngneat/spectator/jest';

import { BookingService } from './booking.service';



describe('BookingsService', () => {
  let spectator: SpectatorHttp<BookingService>;
  const createHttp = createHttpFactory(BookingService)

  beforeEach(() =>spectator = createHttp());

  it('should be created', () => {
    expect(spectator).toBeTruthy();
  });

  it('should call the correct getAllGymClasses URL using a GET HTTP verb', () => {
    spectator.service.getAllGymClasses().subscribe();
    spectator.expectOne(`/nb/gymclasses`, HttpMethod.GET);
  });

  it('should call the correct searchGymClasses URL using a GET HTTP verb', () => {
    spectator.service.searchGymClasses(2).subscribe();
    spectator.expectOne(`/nb/gymclasses/search/2`, HttpMethod.GET);
  });

  it('should call the correct getAllTrainers URL using a GET HTTP verb', () => {
    spectator.service.getAllTrainers().subscribe();
    spectator.expectOne(`/nb/trainers`, HttpMethod.GET);
  });

  it('should call the correct createBooking URL using a GET HTTP verb', () => {
    let gymClassId = 1;
    let bookingStartTime = '2022-02-22T09:00:00';

    spectator.service.makeBooking(gymClassId, bookingStartTime).subscribe();
    spectator.expectOne(`/nb/bookings/create?gymClassId=${gymClassId}&bookingStartTime=${bookingStartTime}`, HttpMethod.POST);
  });


});