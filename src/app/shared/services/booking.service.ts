import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';
import { GymClass } from '../models/gym-class';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private hc: HttpClient) { }

  getAllGymClasses(): Observable<any> {
    return this.hc.get(`/nb/gymclasses`);
  }

  searchGymClasses(trainerId: number): Observable<any> {
    return this.hc.get(`/nb/gymclasses/search/${trainerId}`);
  }

  getAllTrainers(): Observable<any> {
    return this.hc.get(`/nb/trainers`);
  }  

  makeBooking(gymClassId: number, bookingStartTime: string): Observable<any> {
    return this.hc.post(`/nb/bookings/create?gymClassId=${gymClassId}&bookingStartTime=${bookingStartTime}`, null);
  }  
}
