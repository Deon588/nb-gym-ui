import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import { map } from 'rxjs/operators';

import {MatSnackBar} from '@angular/material/snack-bar';

import { BookingDialogComponent } from '../booking-dialog/booking-dialog.component';
import { BookingService } from '../../shared/services/booking.service';
import { GymClass } from '../../shared/models/gym-class'
import { Trainer } from '../../shared/models/trainer';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-available-gym-classes',
  templateUrl: './available-gym-classes.component.html',
  styleUrls: ['./available-gym-classes.component.scss']
})
export class AvailableGymClassesComponent implements OnInit {
  gymClasses: GymClass[] = [];
  trainers: Trainer[] = [];
  busy: boolean = false;
  
  

  constructor(private bookingService: BookingService, public dialog: MatDialog, private snackBar: MatSnackBar) {}

  ngOnInit() {
    this.getGymClasses();
    this.getTrainers();
  }

  getGymClasses() {
    this.busy = true;
    this.bookingService.getAllGymClasses()
    .subscribe((res) => {      
      this.gymClasses =  res;      
      this.busy = false;
    }, (err) => {
      console.log(err);
      this.showError();
      this.busy = false;
    });
  }

  getTrainers() {
    this.busy = true;
    this.bookingService.getAllTrainers()
    .subscribe((res) => {      
      this.trainers = res;
      this.trainers.unshift(new Trainer(99999, "All", "", 0, ""));
      console.log(this.trainers);
      this.busy = false;
    }, (err) => {
      console.log(err);
      this.showError();
      this.busy = false;
    });
  }

  openBookingDialog(gymClass: GymClass) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = 500;
    dialogConfig.data = {
      gymClassName: gymClass.name
    };

    const dialogRef = this.dialog.open(BookingDialogComponent, dialogConfig);

    dialogRef.afterClosed()
  .subscribe((res) => {
    if (res) {    
    this.makeBooking(gymClass.id, moment(res).format('YYYY-mm-DDThh:mm:ss'));
  }
  });
  }

  makeBooking(gymClassId: number, bookingStartTime: string) {
    this.busy = true;
    this.bookingService.makeBooking(gymClassId, bookingStartTime)
    .subscribe((res) => {
      this.busy = false;
      if (res) {
        this.snackBar.open(`Booking Successful`, "", {
          duration: 2000
        });
      }
      console.log(res);
    }, (err) => {      
      this.showError();
      console.log(err);
      this.busy = false;
    })
  }

  filterByTrainer(e: any) {
    console.log(e);
    this.busy = true;
    //hack as I'm running out of time
    if (e.value !== 99999) {
      this.bookingService.searchGymClasses(e.value)
      .subscribe((res) => {
        this.gymClasses = res;
        this.busy = false;
      }, (err) => {
        console.log(err);
        this.showError();
        this.busy = false;
      });
    } else {
      this.getGymClasses();
    }
  }

  showError() {
    this.snackBar.open(`An error has occured! see console for more info`, "", {
      duration: 5000
    });
  }


}
