import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-booking-dialog',
  templateUrl: './booking-dialog.component.html',
  styleUrls: ['./booking-dialog.component.scss']
})
export class BookingDialogComponent implements OnInit {
  gymClassName: string;
  bookingForm: FormGroup;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<BookingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.gymClassName = data.gymClassName;

    this.bookingForm = this.fb.group({    
      bookingDate: [null, Validators.required]    
    });    
   }

  ngOnInit(): void {
  }

  cancel() {
    this.dialogRef.close();
  }

  ok() {
    this.dialogRef.close(this.bookingForm.get('bookingDate').value);
  }

  onSubmit() {}

}
